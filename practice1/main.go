package main

import (
	"../util/radixconv"
	"fmt"
	"os"
)

var usage string = `Usage: ./main toRadix fromRadix num`

func main() {
	if len(os.Args[1:]) < 3 {
		fmt.Fprintf(os.Stderr, "%v", usage)
		os.Exit(1)
	}
	toRadix, err := radixconv.StrToUintBase(os.Args[1], 10)
	if err != nil {
		fmt.Fprintf(os.Stderr, "main: destination radix decode error: %v\n", err)
		os.Exit(1)
	}
	if toRadix < 2 || toRadix > 36 {
		fmt.Fprintf(os.Stderr, "main: invalid destination radix value (%d), must be [2-36]\n", toRadix)
		os.Exit(1)
	}
	fromRadix, err := radixconv.StrToUintBase(os.Args[2], 10)
	if err != nil {
		fmt.Fprintf(os.Stderr, "main: source radix decode error: %v\n", err)
		os.Exit(1)
	}
	if fromRadix < 2 || fromRadix > 36 {
		fmt.Fprintf(os.Stderr, "main: invalid source radix value (%d), must be [2-36]\n", fromRadix)
		os.Exit(1)
	}
	src, err := radixconv.StrToUintBase(os.Args[3], fromRadix)
	if err != nil {
		fmt.Fprintf(os.Stderr, "main: %v\n", err)
		os.Exit(1)
	}
	res, err := radixconv.UintToStrBase(src, toRadix)
	if err != nil {
		fmt.Fprintf(os.Stderr, "main: %v\n", err)
		os.Exit(1)
	}
	// More verbose output
	//fmt.Printf("Convert %s(Base%d)[=%d(Base10)] to Base%d = %s\n",
	//	os.Args[3], fromRadix, src, toRadix, res)
	fmt.Printf("%s\n", res)
}
