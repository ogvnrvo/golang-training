// radixconv package provides utility to convert strings in a given base
// to uint64 value and vice versa.
package radixconv

import (
	"fmt"
)

// StrToUintBase converts ASCII string containing the number in a base
// passed as second parameter to an unsigned integer.
func StrToUintBase(str string, base uint64) (uint64, error) {
	var res uint64
	if base < 2 {
		return 0, fmt.Errorf("radix (%d) cannot be less than 2",
			base)
	}
	for ind, c := range str {
		c, err := checkAndConvert(c, base)
		if err != nil {
			return 0, fmt.Errorf("error at string %q position %d: %v\n",
				str, ind, err)
		}
		if res*base < res {
			return 0, fmt.Errorf("overflow error\n")
		}
		res = res*base + c
	}
	return res, nil
}

// checkAndConvert converts character to an unsigned integer
// and checks whether it fits in base given.
func checkAndConvert(c rune, base uint64) (uint64, error) {
	if c > 127 {
		return 0, fmt.Errorf("invalid character %q", c)
	}
	cb := byte(c)
	if cconv, err := CharToUint(cb); err != nil {
		return 0, err
	} else if uint64(cconv) >= base {
		return 0, fmt.Errorf("invalid character %q for Base%d", cb, base)
	} else {
		return uint64(cconv), nil
	}
}

// UintToStrBase converts unsigned integer to a given base and returns
// the result as string.
func UintToStrBase(val uint64, base uint64) (string, error) {
	var buf string
	if base < 2 {
		return "", fmt.Errorf("radix (%d) cannot be less than 2",
			base)
	}

	for {
		buf = string(UintToChar(uint8(val%base))) + buf
		if val < base {
			break
		}
		val /= base
	}

	return buf, nil
}

// UintToChar converts integer to an ASCII representation.
func UintToChar(val uint8) byte {
	if val <= 9 {
		return val + '0'
	} else {
		return val - 10 + 'a'
	}
}

// CharToUint converts ASCII representation of the character to unsigned
// integer that represents this value.
func CharToUint(val byte) (uint8, error) {
	if !isAllowedChar(val) {
		return 0, fmt.Errorf("invalid character %q", rune(val))
	}
	if val <= '9' {
		return val - '0', nil
	} else {
		return val + 10 - 'a', nil
	}
}

// isAllowedChar inspects byte for relations to range [a-zA-Z0-9].
func isAllowedChar(val byte) bool {
	return val >= '0' && val <= '9' ||
		val&^(1<<5) >= 'A' && val&^(1<<5) <= 'Z'
}
