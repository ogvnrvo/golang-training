package main

import (
	"../util/radixconv"
	"fmt"
	"os"
	"sync"
)

var maxRadix uint64 = 36
var minRadix uint64 = 2
var minWordsPerLine uint64 = 1

// These variables should be guarded by wordsPrintMutex
// unless we are sure that we have exclusive access
var wordsCount uint64
var wordsSep = ""
var wordsPrintMutex = new(sync.Mutex)

// WaitGroup for all goroutines
var wg sync.WaitGroup

var usage string = `Usage: ./main toRadix fromRadix num wordsPerLine`

func main() {
	if len(os.Args[1:]) < 4 {
		fmt.Fprintf(os.Stderr, "%v", usage)
		os.Exit(1)
	}
	toRadix, err := radixconv.StrToUintBase(os.Args[1], 10)
	if err != nil {
		fmt.Fprintf(os.Stderr, "main: destination radix decode error: %v\n", err)
		os.Exit(1)
	}
	if toRadix < minRadix || toRadix > maxRadix {
		fmt.Fprintf(os.Stderr, "main: invalid destination radix value (%d), must be [2-36]\n", toRadix)
		os.Exit(1)
	}
	fromRadix, err := radixconv.StrToUintBase(os.Args[2], 10)
	if err != nil {
		fmt.Fprintf(os.Stderr, "main: source radix decode error: %v\n", err)
		os.Exit(1)
	}
	if fromRadix < minRadix || fromRadix > maxRadix {
		fmt.Fprintf(os.Stderr, "main: invalid source radix value (%d), must be [2-36]\n", fromRadix)
		os.Exit(1)
	}
	src, err := radixconv.StrToUintBase(os.Args[3], fromRadix)
	if err != nil {
		fmt.Fprintf(os.Stderr, "main: %v\n", err)
		os.Exit(1)
	}
	wordsPerLine, err := radixconv.StrToUintBase(os.Args[4], 10)
	if err != nil {
		fmt.Fprintf(os.Stderr, "main: %v\n", err)
		os.Exit(1)
	}
	if wordsPerLine < minWordsPerLine {
		fmt.Fprintf(os.Stderr, "main: invalid words per line value (%d): must be greater than 0\n", wordsPerLine)
		os.Exit(1)
	}

	// Goroutine loop starts each decoding in separate goroutine
	// which in turn runs another goroutine to display the result
	for i := uint64(2); i <= toRadix; i++ {
		wg.Add(1)
		go func(radix uint64) {
			defer wg.Done()
			res, err := radixconv.UintToStrBase(src, radix)
			if err != nil {
				fmt.Fprintf(os.Stderr, "main: %v\n", err)
				os.Exit(1)
			}
			// NOTE: Is it allowed to access waitgroup concurrently?
			wg.Add(1)
			go func() {
				defer wg.Done()
				wordsPrintMutex.Lock()
				fmt.Printf("%s%s", wordsSep, res)
				if wordsCount < wordsPerLine-1 {
					wordsCount++
					wordsSep = ", "
				} else {
					fmt.Printf("\n")
					wordsSep = ""
					wordsCount = 0
				}
				wordsPrintMutex.Unlock()
			}()

		}(i)
	}

	// wait for all goroutines to complete
	wg.Wait()

	// We have an exclusive access: no Mutex acquisition needed
	if wordsCount != 0 {
		fmt.Printf("\n")
	}
}
